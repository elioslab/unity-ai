uusing UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Boid : MonoBehaviour {
	internal FlockController controller;

	internal Vector3 flockCenter;       // Center position of the flock visible by the boid
	internal Vector3 flockVelocity;     // Average Velocity of the flock visible by the boid
	
	internal int flockSize = 0;			// Size of the flock visible by the boid

	// In tue Update() method, we calculate the velocity for our boid using the steer() method
	// and apply it to its rigid body velocity. Next, we check the current speed to verify whether 
	// it's in the range of our controller's maximum and minimum velocity limits. 
	// If not, we cap the velocity at the preset range
	
    void Update() {
        if (controller) {
            Vector3 steer = steer() * Time.deltaTime;

            if(steer != Vector3.zero)
                GetComponent<Rigidbody>().velocity = steer;

            float speed = GetComponent<Rigidbody>().velocity.magnitude;
            if (speed > controller.maxVelocity)
                GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity.normalized * controller.maxVelocity;
            else if (speed < controller.minVelocity)
                GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity.normalized * controller.minVelocity;
        }
    }

	// In the calculateOthers() method, we keep updating the average center and velocity of the
	// flock visible by the boid. Those are the values referenced from our boid object, and they are used to
	// adjust the cohesion and alignment properties with the controller
	
	private void calculateOthers() {
		//Calculate the size, center and velocity of the flock group vidible by the boid
		Vector3 center = Vector3.zero;
		Vector3 velocity = Vector3.zero;
		flockSize = 0;

		foreach (Boid flock in controller.flockList) {
			if ((flock.transform.position - transform.position).magnitude < controller.radius) {
				center += flock.transform.localPosition;
				velocity += flock.GetComponent<Rigidbody>().velocity;
				flockSize++;
			}
		}

		if (flockSize != 0) {
				flockCenter = center / flockSize;
				flockVelocity = velocity / flockSize;
		}
	}

	// The steer() method implements separation, cohesion and alignment rules 
	// of the flocking algorithm, and also a rule to follow the target and to escape from 
	// the obstacle (the other flock).
	// Then, we sum up all the factors together with a random weight value. 
	private Vector3 steer() {
		Vector3 cohesion = Vector3.zero;
		Vector3 alignment = Vector3.zero;
		Vector3 separation = Vector3.zero;
		Vector3 escape = Vector3.zero;
		Vector3 follow = Vector3.zero;
		Vector3 randomize = Vector3.zero;
		
		calculateOthers();

		if(flockSize != 0) {
			cohesion = flockCenter - transform.localPosition;		
			alignment = flockVelocity - GetComponent<Rigidbody>().velocity; 			
			foreach (Boid flock in controller.flockList) {
				if (flock != this) {
					if ((flock.transform.position - transform.position).magnitude < controller.radius) {
						Vector3 relativePos = transform.localPosition - flock.transform.localPosition;
						separation += relativePos / (relativePos.sqrMagnitude);		
					}
				}
			}
		}

		// follow the target
		follow = (controller.target.localPosition - transform.localPosition); 

        // randomize
		randomize = new Vector3( (Random.value * 2) - 1, (Random.value * 2) - 1, (Random.value * 2) - 1);
		
		// escape
		Vector3 relative2Pos = controller.obstacle.flockCenter - transform.localPosition;
		escape = -relative2Pos/(relative2Pos.sqrMagnitude);

		randomize.Normalize();
						
		return (controller.cohesionWeight   * cohesion 		+ 
		        controller.alignmentWeight  * alignment 	+ 
				controller.separationWeight * separation 	+ 
		        controller.followWeight		* follow		+ 
				controller.escapeWeight		* escape		+
				controller.randomizeWeight 	* randomize);
	}	
}