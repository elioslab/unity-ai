using UnityEngine;
using System.Collections;

public class AStar
{
	// We start with our openList declarations which are of the 
	// PriorityQueue 
	
    public static PriorityQueue openList;

	// Next we implement a method called HeuristicEstimateCost to calculate 
	// the cost between the two nodes. The calculation is simple. We just find 
	// the direction vector between the two by subtracting one position vector 
	// from another. The magnitude of this resultant vector gives the direct
	// distance from the current node to the goal node.
	
	private static float HeuristicEstimateCost(Node curNode, Node goalNode)
    {
        Vector3 vecCost = curNode.position - goalNode.position;
        return vecCost.magnitude;
    }
    
	// Next, we have our main FindPath method. We initialize our open list.
	// Starting with the start node, we put it in our open list. 
	// Then we start processing our open list.
	// ThE code implementation resembles the algorithm that we have discussed:
	// 1. Get the first node of our openList. Remember our openList of nodes is 
	// always sorted every time a new node is added. So the first node is always 
	// the node with the least estimated cost to the goal node. 
	// 2. Check if the current node is already at the goal node. If so, exit the 
	// whileloop and build the path array.
	// 3. Create an array list to store the neighboring nodes of the current 
	// node being processed. Use the GetNeighbours method to retrieve the neighbors
	// from the grid.
	// 4. For every node in the neighbors array, we check if it's already in 
	// the OpenList. If not, put it in the calculate the cost values, update the 
	// node properties with the new cost values as well as the parent node data 
	// and put it in openList.
	// 5. Remove it from openList. Go back to step 1.
	// If there are no more nodes in openList, our current node should be at the 
	// target node if there's a valid path available. 
	
    public static ArrayList FindPath(Node start, Node goal)
    {
        openList = new PriorityQueue();
        openList.Push(start);
        start.nodeTotalCost = 0.0f;
        start.estimatedCost = HeuristicEstimateCost(start, goal);

        Node node = null;

		GridManager.instance.reset();

        while (openList.Length != 0)
        {
            node = openList.First();

            if (node.position == goal.position)
            	return CalculatePath(node);
            			
            ArrayList neighbours = new ArrayList();
            GridManager.instance.GetNeighbours(node, neighbours);

            for (int i = 0; i < neighbours.Count; i++)
            {
                Node neighbourNode = (Node)neighbours[i];

				if (neighbourNode.parent == null || node.nodeTotalCost + HeuristicEstimateCost(node, neighbourNode) < neighbourNode.nodeTotalCost)
                {					
	                float cost = HeuristicEstimateCost(node, neighbourNode);	
	                float totalCost = node.nodeTotalCost + cost;
					float neighbourNodeEstCost = HeuristicEstimateCost(neighbourNode, goal);					

	                neighbourNode.nodeTotalCost = totalCost;
	                neighbourNode.parent = node;
	                neighbourNode.estimatedCost = totalCost + neighbourNodeEstCost;

					openList.Push(neighbourNode);
	            }
            }

			openList.Remove(node);
        }
	
        if (node.position != goal.position)
        {
            Debug.LogError("Goal Not Found");
            return null;
        }

        //Calculate the path based on the final node
        return CalculatePath(node);
    }
	
	// The CalculatePath method traces through each node's parent node object and 
	// builds an array list. It gives an array list with nodes from target node to // start node. Since we want a path array from start node to target node we 
	// just call the Reverse method. 
	
	private static ArrayList CalculatePath(Node node)
	{
        ArrayList list = new ArrayList();
        while (node != null)
        {
            list.Add(node);
            node = node.parent;
        }
        list.Reverse();
        return list;
    }
}
