using UnityEngine;
using System.Collections;

public class GridManager : MonoBehaviour
{
    // We'll keep a singleton instance of the GridManager class, as we need 
	// only one object to represent the map. We look for the GridManager object 
	// in our scene and if found, we keep it in our s_Instance static variable.
	
	private static GridManager s_Instance = null;

    public static GridManager instance
    {
        get
        {
           
            {
                s_Instance = FindObjectOfType(typeof(GridManager)) as GridManager;
                if (s_Instance == null)
                    Debug.Log("Could not locate an GridManager object. \n You have to have exactly one GridManager in the scene.");
            }
            return s_Instance;
        }
    }

	// Next, we declare all the variables; we'll need to represent our map, 
	// such as number of rows and columns, the size of each grid tile, 
	// and some boolean variables to visualize the grid and obstacles 
	// as well as to store all the nodes present in the grid 

	public int numOfRows;
    public int numOfColumns;
    public float gridCellSize;
    public bool showGrid = true;
    public bool showObstacleBlocks = true;

    private Vector3 origin = new Vector3();
	public Vector3 Origin {
		get { return origin; }
	}
    private GameObject[] obstacleList;
    public Node[,] nodes { get; set; }
    
    // We look for all the game objects with a tag Obstacle and put them in
	// our obstacleList property. Then we set up our nodes' 2D array in the
	// CalculateObstacles method. First, we just create the normal node objects 
	// with default properties. Just after that we examine our obstacleList.
	// Convert their position into row, column data and update the nodes at that
	// index to be obstacles. The GridManager has a couple of helper methods to 
	// traverse the grid and get the grid cell data. 

    void Awake()
    {
        obstacleList = GameObject.FindGameObjectsWithTag("Obstacle");
        CalculateObstacles();
    }

	public void reset()
	{
		int index = 0;
		for (int i = 0; i < numOfColumns; i++)
		{
			for (int j = 0; j < numOfRows; j++)
			{
				Vector3 cellPos = GetGridCellCenter(index);
				Node node = new Node(cellPos);
				nodes[i,j].parent = null;
			}
		}
	}
	
	void CalculateObstacles()
	{
		nodes = new Node[numOfColumns, numOfRows];
		
		int index = 0;
        for (int i = 0; i < numOfColumns; i++)
        {
            for (int j = 0; j < numOfRows; j++)
            {
                Vector3 cellPos = GetGridCellCenter(index);
                Node node = new Node(cellPos);
                nodes[i, j] = node;

                index++;
            }
        }

        if (obstacleList != null && obstacleList.Length > 0)
        {
            foreach (GameObject data in obstacleList)
            {
                int indexCell = GetGridIndex(data.transform.position);
                int col = GetColumn(indexCell);
                int row = GetRow(indexCell);

                nodes[row, col].MarkAsObstacle();
            }
        }
    }

	// The GetGridCellCenter method returns the position of the grid cell 
	// in world coordinates from the cell index
	
    public Vector3 GetGridCellCenter(int index)
    {
        Vector3 cellPosition = GetGridCellPosition(index);
        cellPosition.x += (gridCellSize / 2.0f);
        cellPosition.z += (gridCellSize / 2.0f);

        return cellPosition;
    }

    public Vector3 GetGridCellPosition(int index)
    {
        int row = GetRow(index);
        int col = GetColumn(index);
        float xPosInGrid = col * gridCellSize;
        float zPosInGrid = row * gridCellSize;

        return Origin + new Vector3(xPosInGrid, 0.0f, zPosInGrid);
    }

	// The GetGridIndex method returns the grid cell index in the grid from the
	// given position:

    public int GetGridIndex(Vector3 pos)
    {
        if (!IsInBounds(pos))
        {
            return -1;
        }

        pos -= Origin;

        int col = (int)(pos.x / gridCellSize);
        int row = (int)(pos.z / gridCellSize);

        return (row * numOfColumns + col);
    }

	// The GetRow and GetColumn methods return the row and column data of the grid
	// cell from the given index: 
	
    public int GetRow(int index)
    {
        int row = index / numOfColumns;
        return row;
    }

    public int GetColumn(int index)
    {
        int col = index % numOfColumns;
        return col;
    }

    public bool IsInBounds(Vector3 pos)
    {
        float width = numOfColumns * gridCellSize;
        float height = numOfRows* gridCellSize;

        return (pos.x >= Origin.x &&  pos.x <= Origin.x + width && pos.x <= Origin.z + height && pos.z >= Origin.z);
    }
	
	// Another important method is GetNeighbours, which is used by the AStar class 
	// to retrieve the neighboring nodes of a particular node. First, we retrieve
	// the neighboring nodes of the current node in the left, right, top, and
	// bottom four directions. Then, inside the AssignNeighbour method, we check 
	// the node to see whether it's an obstacle. If it's not then we push that 
	// neighbor node to the referenced array list, neighbors. 
	
    public void GetNeighbours(Node node, ArrayList neighbors)
    {
        Vector3 neighborPos = node.position;
        int neighborIndex = GetGridIndex(neighborPos);

        int row = GetRow(neighborIndex);
        int column = GetColumn(neighborIndex);

        //Bottom
        int leftNodeRow = row - 1;
        int leftNodeColumn = column;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Top
        leftNodeRow = row + 1;
        leftNodeColumn = column;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Right
        leftNodeRow = row;
        leftNodeColumn = column + 1;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);

        //Left
        leftNodeRow = row;
        leftNodeColumn = column - 1;
        AssignNeighbour(leftNodeRow, leftNodeColumn, neighbors);
    }

    void AssignNeighbour(int row, int column, ArrayList neighbors)
    {
        if (row != -1 && column != -1 && row < numOfRows && column < numOfColumns)
        {
            Node nodeToAdd = nodes[row, column];
            if (!nodeToAdd.bObstacle)
            {
                neighbors.Add(nodeToAdd);
            }
        } 
    }

	// The next method is a debug aid method to visualize the grid and obstacle
	// blocks. Gizmos can be used to draw visual debugging and setup aids inside
	// the editor scene view. OnDrawGizmos is called every frame by the engine. 
	// So, if the debug flags, showGrid and showObstacleBlocks are checked, we just
	//	draw the grid with lines and obstacle cube objects with cubes. 

    void OnDrawGizmos()
    {
        if (showGrid)
        {
            DebugDrawGrid(transform.position, numOfRows, numOfColumns, gridCellSize, Color.blue);
        }

        Gizmos.DrawSphere(transform.position, 0.5f);

        if (showObstacleBlocks)
        {
            Vector3 cellSize = new Vector3(gridCellSize, 1.0f, gridCellSize);

            if (obstacleList != null && obstacleList.Length > 0)
            {
                foreach (GameObject data in obstacleList)
                {
                    Gizmos.DrawCube(GetGridCellCenter(GetGridIndex(data.transform.position)), cellSize);
                }
            }
        }
    }

    public void DebugDrawGrid(Vector3 origin, int numRows, int numCols, float cellSize, Color color)
    {
        float width = (numCols * cellSize);
        float height = (numRows * cellSize);

        for (int i = 0; i < numRows + 1; i++)
        {
            Vector3 startPos = origin + i * cellSize * new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 endPos = startPos + width * new Vector3(1.0f, 0.0f, 0.0f);
            Debug.DrawLine(startPos, endPos, color);
        }

        for (int i = 0; i < numCols + 1; i++)
        {
            Vector3 startPos = origin + i * cellSize * new Vector3(1.0f, 0.0f, 0.0f);
            Vector3 endPos = startPos + height * new Vector3(0.0f, 0.0f, 1.0f);
            Debug.DrawLine(startPos, endPos, color);
        }
    }
}
