using UnityEngine;
using System.Collections;
using System;

public class Node : IComparable
{
    // The Node class has properties, such as the cost values (G and H), 
	// flags to mark whether it is an obstacle, its positions and parent node. 
	// The nodeTotalCost is G, which is the movement cost value from starting 
	// node to this node so far and the estimatedCost is H, which is total 
	// estimated cost from this node to the target goal node. 
	
	public float nodeTotalCost;         
    public float estimatedCost;         
    public bool bObstacle;              
    public Node parent;                 
    public Vector3 position;            

	// We also have two simple constructor methods and a wrapper method to set  
	// whether this node is an obstacle.
	
    public Node()
    {
        this.estimatedCost = 0.0f;
        this.nodeTotalCost = 1.0f;
        this.bObstacle = false;
        this.parent = null;
    }

    public Node(Vector3 pos)
    {
        this.estimatedCost = 0.0f;
        this.nodeTotalCost = 1.0f;
        this.bObstacle = false;
        this.parent = null;

        this.position = pos;
    }

    public void MarkAsObstacle()
    {
        this.bObstacle = true;
    }

	// Then, we implement the CompareTo method. Our Node class inherits from
	// IComparable because we want to override this CompareTo method. We need to
	// sort our list of node arrays based on the total estimated cost. 
	// The ArrayList type has a method called Sort. Sort basically looks for 
	// this CompareTo method, implemented inside the object from the list. 
	 
    public int CompareTo(object obj)
    {
        Node node = (Node)obj;
		if (this.nodeTotalCost < node.nodeTotalCost)
            return -1;
		if (this.nodeTotalCost > node.nodeTotalCost)
            return 1;

        return 0;
    }
}


