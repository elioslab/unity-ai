using UnityEngine;
using System.Collections;

public class PriorityQueue 
{
    // It should be easy to understand. One thing to notice is that after 
	// adding or removing node from the nodes' ArrayList, we call the Sort
	// method. This will call the Node object's CompareTo method, and will 
	// sort the nodes accordingly by the estimatedCost value.
	
	private ArrayList nodes = new ArrayList();

    public int Length
    {
        get { return this.nodes.Count; }
    }

    public bool Contains(object node)
    {
        return this.nodes.Contains(node);
    }

    public Node First()
    {
        if (this.nodes.Count > 0)
        {
            return (Node)this.nodes[0];
        }
        return null;
    }

    public void Push(Node node)
    {
        this.nodes.Add(node);
        this.nodes.Sort();
    }

    public void Remove(Node node)
    {
        this.nodes.Remove(node);
        this.nodes.Sort();
    }
}


