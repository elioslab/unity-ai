using UnityEngine;
using System.Collections;

public class TestCode : MonoBehaviour 
{
    private Transform startPos, endPos;
    public Node startNode { get; set; }
    public Node goalNode { get; set; }

    public ArrayList pathArray;
	
    GameObject objStartCube, objEndCube;
	
    private float elapsedTime = 0.0f;
    public float intervalTime = 1.0f; //Interval time between path finding

	// In the Start method we look for objects with the tags Start and End, and
	// initialize our pathArray as well. We'll be trying to find our new path at
	// every interval that we set to our intervalTime property in case the
	//positions of the start and end nodes have changed. 
	
	void Start () 
    {
        objStartCube = GameObject.FindGameObjectWithTag("Start");
        objEndCube = GameObject.FindGameObjectWithTag("End");

        //AStar Calculated Path
        pathArray = new ArrayList();
        FindPath();
	}
	
	void Update () 
    {
        elapsedTime += Time.deltaTime;

        if(elapsedTime >= intervalTime)
        {
            elapsedTime = 0.0f;
            FindPath();
        }
	}

	// Since we implemented our pathfinding algorithm in the AStar class, finding 
	// a path has now become a lot simpler. First, we take the positions of our
	// start and end game objects. Then, we create new Node objects using the 
	// helper methods of GridManager, GetGridIndex, to calculate their respective 
	// row and column index positions inside the grid. Once we get that we just 
	// call the AStar.FindPath method with the start node and goal node, and store 
	// the returned array list in the local pathArray property.

    void FindPath()
    {
        startPos = objStartCube.transform;
        endPos = objEndCube.transform;

        //Assign StartNode and Goal Node
        startNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(startPos.position)));
        goalNode = new Node(GridManager.instance.GetGridCellCenter(GridManager.instance.GetGridIndex(endPos.position)));

        pathArray = AStar.FindPath(startNode, goalNode);
    }

	
	// Next we implement the OnDrawGizmos method to draw and visualize the 
	// path found. We look through our pathArray and use the Debug.DrawLine method 
	// to draw the lines connecting the nodes from the pathArray. With that we'll 
	// be able to see a green line connecting the nodes from start to end forming a 
	// path.
	
    void OnDrawGizmos()
    {
        if (pathArray == null)
            return;

        if (pathArray.Count > 0)
        {
            int index = 1;
            foreach (Node node in pathArray)
            {
                if (index < pathArray.Count)
                {
                    Node nextNode = (Node)pathArray[index];
                    Debug.DrawLine(node.position, nextNode.position, Color.green);
                    index++;
                }
            };
        }
    }
}