using System;
using System.Collections;
using System.Data;

public class Attribute
{
	ArrayList mValues;
	String mName;
	object mLabel;

	public Attribute(String name, String[] values)
	{
		mName = name;
		mValues = new ArrayList(values);
		mValues.Sort();
	}

	public Attribute(object Label)
	{
		mLabel = Label;
		mName = String.Empty;
		mValues = null;
	}

	public String AttributeName
	{
		get
		{
			return mName;
		}
	}
		
	public String[] values
	{
		get
		{
			if (mValues != null)
				return (String[])mValues.ToArray(typeof(String));
			else
				return null; 
		}
	}

	public bool isValidValue(String value)
	{
		return indexValue(value) >= 0;
	}

	public int indexValue(String value)
	{
		if (mValues != null)
			return mValues.BinarySearch(value);
		else
			return -1;
	}

	public override String ToString()
	{
		if (mName != String.Empty)
		{
			return mName;
		}
		else
		{
			return mLabel.ToString();
		}
	}
}




