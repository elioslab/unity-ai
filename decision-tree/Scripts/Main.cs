using System;
using UnityEngine;
using System.Collections;
using System.Data;

public class Main : MonoBehaviour
{
	public TreeNode createDecisionTreeExample()
	{
		Attribute a1 = new Attribute("num_minerals", new string[] {"Low", "High"});
		Attribute a2 = new Attribute("num_gas", new string[] {"Low", "High"});
		Attribute a3 = new Attribute("access", new string[] {"Yes", "No"});
		Attribute a4 = new Attribute("distance", new string[] {"Close", "Far"});
		
		Attribute[] attributes = new Attribute[] {a1, a2, a3, a4};

		DataTable result = new DataTable("test");
		DataColumn column = result.Columns.Add("num_minerals");
		column.DataType = typeof(String);
		column = result.Columns.Add("num_gas");
		column.DataType = typeof(String);
		column = result.Columns.Add("access");
		column.DataType = typeof(String);
		column = result.Columns.Add("distance");
		column.DataType = typeof(String);
		column = result.Columns.Add("strategy");
		column.DataType = typeof(bool);
		
		//minerals, gas, access, distance, priority defence?
		result.Rows.Add(new object[] {"Low","High","No","Far",true});
		result.Rows.Add(new object[] {"Low","High","Yes","Far",true});
		result.Rows.Add(new object[] {"Low","Low","No","Far",false});
		result.Rows.Add(new object[] {"Low","Low","Yes","Far",false});
		result.Rows.Add(new object[] {"High","High","No","Far",true});
		result.Rows.Add(new object[] {"High","High","Yes","Far",true});
		result.Rows.Add(new object[] {"High","Low","No","Far",true});
		result.Rows.Add(new object[] {"High","Low","Yes","Far",true});
		result.Rows.Add(new object[] {"Low","High","No","Close",true});
		result.Rows.Add(new object[] {"Low","High","Yes","Close",false});
		result.Rows.Add(new object[] {"Low","Low","No","Close",false});
		result.Rows.Add(new object[] {"Low","Low","Yes","Close",false});
		result.Rows.Add(new object[] {"High","High","No","Close",true});
		result.Rows.Add(new object[] {"High","High","Yes","Close",true});
		result.Rows.Add(new object[] {"High","Low","No","Close",true});
		result.Rows.Add(new object[] {"High","Low","Yes","Close",false});
		
		DecisionTree tree = new DecisionTree();
		TreeNode root = tree.mountTree(result, "strategy", attributes);
		return root;
	}

	public String printNode(TreeNode root, String tabs, String p)
	{
		String printout;
		printout = " |" + root.attribute + "|\n";
		
		if (root.attribute.values != null)
		{
			for (int i = 0; i < root.attribute.values.Length; i++)
			{
				printout += tabs + "<" + root.attribute.values[i] + ">";
				TreeNode childNode = root.getChildByBranchName(root.attribute.values[i]);
				printout += printNode(childNode, tabs + "\t", printout);
			}
		}
		return printout;
	}

	void Start()
	{
		TreeNode root = createDecisionTreeExample ();
		String treeString = "";
		treeString = printNode(root, "\t", treeString);
		Debug.Log(treeString); 
	}
}



