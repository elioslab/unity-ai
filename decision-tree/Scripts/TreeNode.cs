using System;
using System.Collections;
using System.Data;

public class TreeNode
{
	private ArrayList mChilds = null;
	private Attribute mAttribute;

	public TreeNode(Attribute attribute)
	{
		if (attribute.values != null)
		{
			mChilds = new ArrayList(attribute.values.Length);
			for (int i = 0; i < attribute.values.Length; i++)
				mChilds.Add(null);
		}
		else
		{
			mChilds = new ArrayList(1);
			mChilds.Add(null);
		}
		mAttribute = attribute;
	}

	public void AddTreeNode(TreeNode treeNode, String ValueName)
	{
		int index = mAttribute.indexValue(ValueName);
		mChilds[index] = treeNode;
	}

	public int totalChilds 
	{
		get
		{
			return mChilds.Count;
		}
	}


	public TreeNode getChild(int index)
	{
		return (TreeNode)mChilds[index];
	}

	public Attribute attribute
	{
		get
		{
			return mAttribute;
		}
	}

	public TreeNode getChildByBranchName(String branchName)
	{
		int index = mAttribute.indexValue(branchName);
		return (TreeNode)mChilds[index];
	}
}



