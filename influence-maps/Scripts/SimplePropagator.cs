using UnityEngine;
using System.Collections;

public interface IPropagator
{
	Vector2I GridPosition { get; }
	float Value { get; }
}

public class SimplePropagator : MonoBehaviour, IPropagator
{
	[SerializeField]
	float value;
	public float Value { get{ return value; } }

	[SerializeField]
	InfluenceMapControl map;
	CharacterController character;

	Vector3 bottomLeft;
	Vector3 topRight;
	Vector3 destination;
	Vector3 velocity;

	[SerializeField]
	float speed;

	public Vector2I GridPosition
	{
		get	{ return map.GetGridPosition(transform.position); }
	}

	void Start()
	{
		character = GetComponent<CharacterController>();
		map.RegisterPropagator(this);
		map.GetMovementLimits(out bottomLeft, out topRight);

		InvokeRepeating("ChooseNewDestination", 0.001f, 3.0f);
	}

	void Update()
	{
		velocity = destination - transform.position;
		velocity.Normalize();
		velocity *= speed;

		character.SimpleMove(velocity);
	}

	void ChooseNewDestination()
	{
		destination = new Vector3(Random.Range(bottomLeft.x, topRight.x), Random.Range(bottomLeft.y, topRight.y), Random.Range(bottomLeft.z, topRight.z));
	}
}
