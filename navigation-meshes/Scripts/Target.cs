using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour
{
    private UnityEngine.AI.NavMeshAgent[] navAgents;
    public Transform targetMarker;

    void Start ()
    {
		// we look for all the NavMeshAgent type entities in our game
		// and store them in a reference NavMeshAgent array. 
		navAgents = FindObjectsOfType(typeof(UnityEngine.AI.NavMeshAgent)) as UnityEngine.AI.NavMeshAgent[];
    }

    void UpdateTargets ( Vector3 targetPosition  )
    {
	    foreach(UnityEngine.AI.NavMeshAgent agent in navAgents) 
        {
		    agent.destination = targetPosition;
	    }
    }

    void Update ()
    {
        int button = 0;

		if(Input.GetMouseButtonDown(button)) 
        {
			// Whenever there's a mouseclick event, we do a simple raycast to determine the first 
			// objects that collide with the ray. If the ray hits any object, we update the position 
			// of our marker and update each navmesh agent's destination by setting the destination 
			// property with the new position. 

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray.origin, ray.direction, out hitInfo)) 
            {
                Vector3 targetPosition = hitInfo.point;
                UpdateTargets(targetPosition);
				targetMarker.position = targetPosition;
            }
        }
    }
}